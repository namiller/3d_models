wall = 3;
thin_wall = .35;
height = 15;
width = 20;
length = 35;
radius = 1.8;

module box() {
  union() {
    difference() {
      difference() {
        cube([width, length, height], true);
        cube([width-wall*2, length-wall*2, height-wall*2], true);
      }
      union() {
        rotate([90,0,0]) {cylinder(length + .05, r1=radius, r2=radius, $fn=99, center=true);}
        translate([5,0,0]) {cube([height, 7.2, 2.4], true);}
      }
    }
    difference() {
      translate([-2.3/2+width/2-thin_wall,0,0]) {cube([2.3+2*thin_wall,7.2+2*thin_wall,2.4+2*thin_wall], true);}
      union() {
        translate([-2.3/2+width/2+.05,0,0]) {cube([2.3+.1,7.2,2.4], true);}
        translate([5,0,0]) cube([height, 5.6, .6], center=true);
      }
    }
  }
}

module seam(thickness, height, margin) {
  translate([-width/2 + wall/2, 0, 0]) cube([thickness, length-wall+thickness, height], center=true);
  difference() {
    translate([width/2 - wall/2, 0, 0]) cube([thickness, length-wall+thickness, height], center=true);
    translate([-2.3/2+width/2+.05,0,0]) {cube([2.3+.1,7.2-margin,2.4], true);}
  }
  difference() {
    union() {
      translate([0,length/2 - wall/2, 0]) cube([width-wall+thickness, thickness, height], center=true);
      translate([0,-length/2 + wall/2, 0]) cube([width-wall+thickness, thickness, height], center=true);
    }
    rotate([90,0,0]) {cylinder(length + .05, r1=radius-margin, r2=radius-margin, $fn=99, center=true);}
  }

}

module bottom(thickness, height) {
  union() {
    difference() {
      box();
      translate([0,0,25-height/2]) cube([50,50,50], true);
    }
    seam(thickness-.1, height, 0);
  }
}

module top(thickness, height) {
  difference() {
    difference() {
      box();
      translate([0,0,25+height/2-.05]) cube([50,50,50], true);
    }
    seam(thickness, height,.05);
  }
}

translate([-width / 2 - 5,0,height/2]) bottom(1,.5);
translate([width / 2 + 5,0,height/2]) top(1, .5);
